var express = require('express');
var router = express.Router();

/* GET transactions listing. */
router.get('/', async function(req, res, next) {
  const response = await fetch('http://internal-clementino-internal-alb-1577108199.eu-west-2.elb.amazonaws.com/customers');
  // const response = await fetch('http://custodio.custodio.local/customers');
  const customers = await response.json();
  res.send(customers)
});

module.exports = router;