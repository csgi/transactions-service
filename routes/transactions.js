var express = require('express');
var router = express.Router();

/* GET transactions listing. */
router.get('/', function(req, res, next) {
  res.send([
    {
      id: 1,
      customerId: 1,
      value: 100,
      currency: 'Euro'
    },
    {
      id: 2,
      customerId: 3,
      value: 10,
      currency: 'Euro'
    },
    {
      id: 3,
      customerId: 2,
      value: 1000,
      currency: 'Euro'
    },
    {
      id: 4,
      customerId: 1,
      value: 1,
      currency: 'Euro'
    },
    {
      id: 5,
      customerId: 2,
      value: 10000,
      currency: 'Euro'
    },
  ]);
});

module.exports = router;
